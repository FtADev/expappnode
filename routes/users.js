const express = require('express')
const router = express.Router()
const axios = require('axios').default
const response = require('../response')
const md5 = require('md5')

let MongoClient = require('mongodb').MongoClient
let url = 'mongodb+srv://exception:exception@cluster0-fslwt.mongodb.net/'

/* GET users listing. */
router.post('/login', function (req, res, next) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err
    let dbo = db.db('ExceptionDB')
    let query = {username: req.body.username, password: md5(req.body.password)}
    dbo.collection('Users').findOne(query, function (err, result) {
      if (err) throw err
      if (result != null)
        res.send(result)
      else
        res.send(response.nullUser)
      db.close()
    })
  })
})

router.post('/register', function (req, res, next) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err
    let dbo = db.db('ExceptionDB')
    let query = {api_key: req.body.api_key}
    let newValues = {
      $set: {
        username: req.body.username,
        password: md5(req.body.password),
      },
    }
    let promise = new Promise(function (resolve, reject) {
      dbo.collection('Users').updateOne(query, newValues, function (err, result) {
          if (err) throw err
          resolve(result.result.nModified)
          console.log(result.result.nModified)
        })
    })
    promise.then(function (value) {
      if (value === 1) {
        console.log(req.body.api_key)
        let query = {api_key: req.body.api_key}
        dbo.collection('Users').findOne(query, function (err, result) {
          console.log("HEHEH")
          if (err) console.log(err.message)
          if (result != null)
            res.send(result)
          else
            res.send(response.nullUser)
          db.close()
        })
      }
      else
        console.log("AHHHH")
    })
  })
})

router.get('/getStatus', function (req, res, next) {
  let api_key = req.query.api_key
  // let api_key = '5ca35dfb-4691-440b-b8a8-fb6f419920d0';
  let today = new Date()
  let start = new Date(new Date().setDate(new Date().getDate() - 14))
  axios.get('https://wakatime.com/api/v1/users/current/summaries', {
    params: {
      start: start.getFullYear() + '-' + (start.getMonth() + 1) + '-' +
        start.getDate(),
      end: today.getFullYear() + '-' + (today.getMonth() + 1) + '-' +
        today.getDate(),
      api_key: api_key,
    },
  }).then(function (response) {
    console.log(response.data)
    res.send(response.data)
  }).catch(function (error) {
    console.log(error)
  }).finally(function () {
    // always executed
  })
})

router.get('/getStatusToday', function (req, res, next) {
  let api_key = req.query.api_key
  // let api_key = '5ca35dfb-4691-440b-b8a8-fb6f419920d0';
  let today = new Date()
  // let start = new Date(new Date().setDate(new Date().getDate() - 1));
  axios.get('https://wakatime.com/api/v1/users/current/summaries', {
    params: {
      start: today.getFullYear() + '-' + (today.getMonth() + 1) + '-' +
        today.getDate(),
      end: today.getFullYear() + '-' + (today.getMonth() + 1) + '-' +
        today.getDate(),
      api_key: api_key,
    },
  }).then(function (response) {
    console.log(response.data)
    res.send(response.data)
  }).catch(function (error) {
    console.log(error)
  }).finally(function () {
    // always executed
  })
})

router.get('/userStatus/:userId/:start/:end', function (req, res, next) {
  let promise = new Promise(function (resolve, reject) {
    MongoClient.connect(url, function (err, db) {
      if (err) throw err
      var dbo = db.db('ExceptionDB')
      var query = {userId: req.params.userId}
      dbo.collection('Users').findOne(query, function (err, result) {
        if (err) throw err
        console.log(result.fullName)
        resolve(result.api_key)
        db.close()
      })
    })
  })

  promise.then(function (value) {
    axios.get('https://wakatime.com/api/v1/users/current/summaries', {
      params: {
        start: req.params.start,
        end: req.params.end,
        api_key: value,
      },
    }).then(function (response) {
      console.log(response.data)
      res.send(response.data)
    }).catch(function (error) {
      console.log(error)
    }).finally(function () {
      // always executed
    })
  })

})

function getUsers (start, end) {
  return new Promise(function (resolve, reject) {
    MongoClient.connect(url, function (err, db) {
      if (err) throw err
      let dbo = db.db('ExceptionDB')
      dbo.collection('Users').find({}, function (err, result) {
        if (err) throw err
        let promises = []
        result.forEach(function (item, index) {
          promises.push(
            axios.get('https://wakatime.com/api/v1/users/current/summaries', {
              params: {
                start,
                end,
                api_key: item.api_key,
              },
            }))
        }).then(function () {
            Promise.all(promises).then(function (values) {
              let userList = []
              console.log(result)

              result.forEach(function (item, index) {
                console.log(item)
                userList.push(
                  {
                    'fullName': item.fullName,
                    'userId': item.userId,
                    'team': item.team,
                    'userData': values[item.userId - 1].data,
                  },
                )
              }).then(function () {
                resolve(userList)
              })
            }).catch(function () {
              reject()
            }).finally(() => {
              db.close()
            })
          },
        ).catch(function () {
          reject()
        })
      })
    })
  })
}

router.get('/getAllUser/:start/:end', function (req, res, next) {
  getUsers(req.params.start, req.params.end).then(function (value) {
    console.log('Hello?')
    res.send({
      'users': value,
    })
  }).catch(function () {
    console.log('lol?')
  })
})

function getUsers97 (start, end) {
  return new Promise(function (resolve, reject) {
    MongoClient.connect(url, function (err, db) {
      if (err) throw err
      let dbo = db.db('ExceptionDB')
      dbo.collection('Users97').find({}, function (err, result) {
        if (err) throw err
        let promises = []
        result.forEach(function (item, index) {
          promises.push(
            axios.get('https://wakatime.com/api/v1/users/current/summaries', {
              params: {
                start,
                end,
                api_key: item.api_key,
              },
            }))
        }).then(function () {
            Promise.all(promises).then(function (values) {
              let userList = []
              console.log(result)

              result.forEach(function (item, index) {
                console.log(item)
                userList.push(
                  {
                    'fullName': item.fullName,
                    'userId': item.userId,
                    'team': item.team,
                    'userData': values[item.userId - 1].data,
                  },
                )
              }).then(function () {
                resolve(userList)
              })
            }).catch(function () {
              reject()
            }).finally(() => {
              db.close()
            })
          },
        ).catch(function () {
          reject()
        })
      })
    })
  })
}

router.get('/getAllUser97/:start/:end', function (req, res, next) {
  getUsers97(req.params.start, req.params.end).then(function (value) {
    console.log('Hello?')
    res.send({
      'users': value,
    })
  }).catch(function () {
    console.log('lol?')
  })
})

module.exports = router
