let express = require('express')
const CronJob = require('cron').CronJob
const Moment = require('moment')
const axios = require('axios').default
const summaries = require('../summaries')
let router = express.Router()
let MongoClient = require('mongodb').MongoClient
let url = 'mongodb+srv://exception:exception@cluster0-fslwt.mongodb.net/'

console.log('Before job instantiation')
const job = new CronJob('0 */5 * * * *', function () {
  // let options = { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric', timeZone: "Asia/Tehran" };
  // const d = new Date().toLocaleDateString("en-US", options);
  let todayTime = Moment().tz('Asia/Tehran').format('YYYY-MM-DD, HH:mm:ss')
  let today = Moment().tz('Asia/Tehran').format('YYYY-MM-DD')
  let yesterday = Moment().tz('Asia/Tehran').subtract(1, 'days').format('YYYY-MM-DD')
  console.log('Every Minutes:', todayTime)
  // getAllUserState(yesterday, today)
  // getAllUser97State(yesterday, today)
}, 'Asia/Tehran')
console.log('After job instantiation')
job.start()

function getAllUserState (start, end) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err
    let dbo = db.db('ExceptionDB')
    dbo.collection('Users').find({}, function (err, result) {
      if (err) throw err
      let promises = []
      result.forEach(function (item, index) {
        promises.push(
          axios.get('https://wakatime.com/api/v1/users/current/summaries', {
            params: {
              start,
              end,
              api_key: item.api_key,
            },
          }))
      }).then(function () {
          Promise.all(promises).then(function (values) {
            result.forEach(function (item, index) {

            }).then(function () {

            })
          }).catch(function () {
          }).finally(() => {
            db.close()
          })
        },
      ).catch(function () {
      })
    })
  })
}

function getAllUser97State (start, end) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err
    let dbo = db.db('ExceptionDB')
    dbo.collection('Users97').find({}, function (err, result) {
      if (err) throw err
      let promises = []
      result.forEach(function (item, index) {
        promises.push(
          axios.get('https://wakatime.com/api/v1/users/current/summaries', {
            params: {
              start,
              end,
              api_key: item.api_key,
            },
          }))
      }).then(function () {
          Promise.all(promises).then(function (values) {
            result.forEach(function (item, index) {
              //Update DB
            }).then(function () {

            })
          }).catch(function () {
          }).finally(() => {
            db.close()
          })
        },
      ).catch(function () {
      })
    })
  })
}

function insert () {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err
    let dbo = db.db('ExceptionDB')
    let durationDataList = [
      {
        'userId': '',
        'fullName': '',
        'team': '',
        'api_key': '',
      },
    ]
    dbo.collection('Users97').insertMany(durationDataList, function (err, res) {
      if (err) throw err
      console.log('Number of documents inserted: ' + res.insertedCount)
    })

    db.close()
  })
}

function insertSummaries() {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err
    let dbo = db.db('ExceptionDB')
    dbo.collection('Summaries').insertOne(summaries, function (err, res) {
      if (err) throw err
      console.log('Number of documents inserted: ' + res.insertedCount)
    })

    db.close()
  })
}

module.exports = router
