let createError = require('http-errors')
let express = require('express')
let cors = require('cors')
let path = require('path')
let cookieParser = require('cookie-parser')
let logger = require('morgan')
const config = require('./config/config')

let indexRouter = require('./routes/index')
let usersRouter = require('./routes/users')

let corsOptions = {
    origin: function (origin, callback) {    // allow requests with no origin
        // (like mobile apps or curl requests)
        if (!origin) return callback(null, true);
        // if (allowedOrigins.indexOf(origin) === -1) {
        //     var msg = 'The CORS policy for this site does not ' +
        //         'allow access from the specified Origin.';
        //     return callback(new Error(msg), false);
        // }
        return callback(null, true);
    }
}

let app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(cors(corsOptions));
app.options('*', cors(corsOptions))
app.use('/', indexRouter)
app.use('/users', usersRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.render('error')
})

app.listen(config.port,
    () => console.log(`App listening on port ${config.port}!`))

module.exports = app
