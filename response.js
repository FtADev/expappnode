let ok = {
  'code': '0',
  'message': 'everything is alright',
}

let oops = {
  'code': '1',
  'message': 'Oops!',
}

let nullUser = {
  "_id": null,
  "userId": null,
  "fullName": null,
  "team": null,
  "api_key": null
}

module.exports = {
  ok,
  oops,
  nullUser
}
